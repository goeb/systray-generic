=======================================================================================
systray-generic
=======================================================================================

---------------------------------------------------------------------------------------
System tray icon(s) to run arbitrary commands.
---------------------------------------------------------------------------------------

:Author:         Stefan Göbel <systray-generic at subtype dot de>
:Date:           2015/10/12
:Version:        2015101201
:Manual section: 1
:Manual group:   User Commands

SYNOPSIS
=======================================================================================

**systray-generic** *[options]* *<configuration file>* *…*

DESCRIPTION
=======================================================================================

**systray-generic** is a simple Python script that displays one or more system tray
icons and runs arbitrary commands when an icon is triggered (e.g. clicked). The icon
can be changed during runtime, based on the output of a command executed when the icon
is clicked, repeatedly at a certain configurable interval or whenever a *SIGUSR1* or a
*SIGUSR2* signal is received. Different commands can be configured for different states
of the system tray icon. See the *CONFIGURATION FILE* section below for a more detailed
explanation.

Note that one instance of **systray-generic** is not restricted to one configuration
file. If more than one configuration file is specified on the command line, one system
tray icon will be created for every file, and almost all settings are specific to the
respective system tray icon. However, it is also possible to just run multiple
**systray-generic** instances.

To close the system tray icon(s), right click the icon and select *Exit*. If multiple
icons are displayed (using multiple configuration files with one **systray-generic**
instance), every icon provides the same menu, closing individual icons while keeping
others running is not supported at the moment. You have to run multiple instances if
this is required.

Note that any errors, e.g. an invalid configuration file or an error executing an
external command, will cause **systray-generic** to quit. All error messages are sent
to *STDERR*, you may want to start **systray-generic** from a terminal to read them.

OPTIONS
=======================================================================================

-h, --help
   Show a short help message and exit.

-v, --version
   Show version and license information and exit.

CONFIGURATION FILE
=======================================================================================

Syntax
---------------------------------------------------------------------------------------

**systray-generic** uses an INI-style configuration file, the general format is::

   [section]
   key = value

Python's ``configparser`` module is used to parse the configuration file, using the
``ExtendedInterpolation`` handler. You can refer to settings defined in any section
within the configuration file itself using the ``${section:key}`` syntax (note that the
*SECTION:key* syntax is also used in this document). Multiline strings are supported by
indenting continuation lines. Section names (and values) are case-sensitive, the keys
are case-insensitive.

Please see the documentation at https://docs.python.org/3/library/configparser.html for
more details.

It is valid to define keys that are not mentioned below in any section, however these
may conflict with options in future versions of **systray-generic**. It is recommended
to use a custom prefix (e.g. an underscore: ``_``) for custom options.

States
---------------------------------------------------------------------------------------

A system tray icon created by **systray-generic** always has a specific state. Several
options are associated with each state, defined in the configuration file. In general,
whenever a state is triggered, either by clicking the system tray icon, or, if enabled,
receiving a *SIGUSR1* or *SIGUSR2* signal or automatically by a timer, the command
associated with the current state (or the state configured for the signals and/or the
automatic update) is executed, and the command's output determines the new state of the
system tray icon.

The states are defined in the configuration file as sections, and the section names are
the names of the states::

   [state name]

The names of the states may be arbitrary strings. It is recommended to not use all
capital letters for state names, to avoid clashes with future special states. At the
moment, the following sections/names have a special meaning:

*CONFIG*

   This is not a state. The ``[CONFIG]`` section may contain several general
   configuration options for **systray-generic**, see below.

*INIT*

   The ``[INIT]`` section defines the command to determine the initial state of the
   system tray icon. It is the only section **required** in every configuration file.
   It will also be used for automatic updates, unless the configuration also contains
   an ``[UPDATE]`` section.

*UPDATE*

   This section sets the command to be used for the automatic (timer-based) updates. If
   these updates are enabled (i.e. when the *CONFIG:update* option is set), and the
   configuration file does not include an ``[UPDATE]`` section, the command in the
   ``[INIT]`` section will be used instead.

*EXIT*

   This is not a state. If the new state (determined as described below either from the
   command output or the *next_state* option) is ``EXIT``, **systray-generic** will
   quit. If *CONFIG:signal_usr1* and/or *CONFIG:signal_usr2* are set to ``EXIT``,
   receiving the respective signal will also cause **systray-generic** to quit.

State Specific Settings
---------------------------------------------------------------------------------------

To set the command to be executed when a state is triggered the following options may
be used. These are valid in every state section, including the ``[INIT]`` section (to
set the command to get the initial state) and the ``[UPDATE]`` section (to set the
command used for automatic updates at a certain interval).

*command*

   The command to run to get the next state (unless overridden by the *next_state*
   option, see below). If specified, the command must print the name of the next state
   of the system tray icon to its standard output. All leading whitespace characters
   will be removed (including leading newline characters), and everything after the
   first line break (after the state name) will be ignored. The command must exist and
   exit without error (i.e. exit code ``0``). Note that **systray-generic** will always
   wait for a command to finish before continuing, unless the *background* option is
   set to ``true``. *command* is an optional setting. If it is not set, or does not
   provide the required output, or *background* is set to ``true``, *next_state* must
   be set!

*use_shell*

   Determines whether the shell will be used (if set to ``true``) or not (if set to
   ``false``, this is the default). If set to ``true``, arbitrary shell code may be
   used as the *command*, it will be passed to the shell as one string. If set to
   ``false``, the *command* will be executed directly after splitting the parameters
   (using Python's ``shlex.split()`` function). Quoting and escaping may be used as
   required.

*background*

   If this is set to ``true``, **systray-generic** will execute the command in the
   background and not wait for it to finish. The output of the command and its exit
   status will be ignored in this case. If this is set to ``true``, the *next_state*
   option has to be set! The default value of the *background* option is ``false``.

*next_state*

   Explicitely set the name of the new state of the system tray icon. If this is set,
   the *command* will still be run as configured (and **systray-generic** will wait for
   it to finish unless the *background* option is enabled), but its output will be
   ignored and the new state will be the state set by this option.

**systray-generic** will exit if the new state is ``EXIT``, every other value will just
set the new state. A state with the determined name must exist in the configuration
file!

Note that both ``INIT`` and ``UPDATE`` are also valid state names, i.e. the new state
can be ``INIT`` or ``UPDATE``, and will be treated as any other state in that case.

Every regular state section must include an *icon* setting, which defines the actual
system tray icon to be displayed. If ``INIT`` and/or ``UPDATE`` are used as regular
states, the *icon* option is mandatory for them, too, otherwise these sections do not
require an *icon* to be set.

*icon*

   Path to an image file that should be used as the system tray icon for the state it
   is defined in. All image formats supported by *QIcon* are supported.

General Configuration Options
---------------------------------------------------------------------------------------

The following options are recognized in the ``[CONFIG]`` section of the configuration
file:

*process_name*

   Since **systray-generic** is just a Python script, it would show up in the output of
   tools like **ps(1)** simply as ``python``. The value of this setting will replace
   the generic ``python`` name. Setting this is optional, and the default value is
   ``systray-generic``. Note that any arbitrary string may be set (even an empty
   string), but values longer than 15 characters will be truncated. You should set this
   to a unique value if you want to run multiple instances of the script and be able to
   easily identify each one with tools like **ps(1)**. If **systray-generic** is run
   with more than one configuration file, the first *process_name* setting found will
   be used, subsequent settings will be ignored.

*signal_usr1*, *signal_usr2*

   When the **systray-generic** process receives a *SIGUSR1* or *SIGUSR2* signal, the
   command associated to the state specified by these options will be run (if any) and
   the state will be changed according to its output, or the *next_state* setting of
   the state. This means that if - for example - *signal_usr1* is set to ``state1``,
   receiving the *SIGUSR1* signal will be treated the same as a click on the system
   tray icon when the system tray icon's state is ``state1``. If executing a command is
   not required for any signal, a state without a command (but a *next_state* option)
   may be created as appropriate. These options may also be set to ``EXIT``, in which
   case the respective signals will cause **systray-generic** to quit. Leaving these
   options empty will cause the signals to be ignored by **systray-generic**.

*signal_interval*

   The interval at which to check for *SIGUSR1* or *SIGUSR2* signals, in milliseconds.
   Note that any other event, for example a click on the system tray icon or the timer
   for the automatic update, will also trigger the check for signals. The default value
   is ``500`` (half a second). The value must be a positive integer number. If multiple
   configuration files are used the lowest setting (excluding the default value) will
   determine the interval.

*update*

   If this option is set to a valid integer greater than zero, **systray-generic** will
   recheck the system tray state every *update* milliseconds using the settings in the
   ``[UPDATE]`` section or - if no ``[UPDATE]`` section exists - the settings in the
   ``[INIT]`` section. It will be handled as any other state trigger, i.e. either the
   command output or the *next_state* setting will determine the system tray icon's new
   state.

Example Configuration
---------------------------------------------------------------------------------------

An example similar to the following is included in the **systray-generic** distribution
(including the required icons)::

   [CONFIG]

   process_name    = nv-gpufreq-tray
   update          = 10000
   signal_usr1     = auto
   signal_usr2     = maximum_performance
   signal_interval = 250

   [INIT]

   command         = ( nvidia-settings -q '[gpu:0]/GPUPowerMizerMode' |
                       grep "Attribute 'GPUPowerMizerMode'.*: 2" >/dev/null 2>&1
                     ) && echo 'auto' || echo 'maximum_performance'
   use_shell       = true

   [auto]

   command         = nvidia-settings -a [gpu:0]/GPUPowerMizerMode=1
   icon            = gpufreq-auto.svg
   next_state      = maximum_performance

   [maximum_performance]

   command         = nvidia-settings -a [gpu:0]/GPUPowerMizerMode=2
   icon            = gpufreq-performance.svg
   next_state      = auto

This example will show the Nvidia PowerMizer level of the first GPU in the system tray,
and clicking the icon will toggle the state between *Auto* and *Maximum Performance*.

In the ``[INIT]`` section the current PowerMizer state is determined using a small
shell script (multiline using indentation), printing either ``maximum_performance`` or
``auto`` to standard output, and thus setting the initial state of the system tray icon
to ``auto`` or ``maximum_performance``, respectively.

The *update* option is set to ``10000``, and since there is no separate ``[UPDATE]``
section the same shell script will be run every 10 seconds to update the state (so the
system tray icon will reflect any changes made by other programs).

If the system tray icon is in the ``auto`` state, clicking it will run the command in
the ``[auto]`` section to set the PowerMizer mode to maximum performance, and vice
versa. The tray icon will change appropriately.

Additionally, the option *signal_usr1* is set to ``auto``, so if a *SIGUSR1* signal is
received it will also trigger the ``auto`` section's command and the PowerMizer state
will be changed to maximum performance, and a *SIGUSR2* signal will switch it back to
auto.

If, for example, the *signal_usr1* option would be set to ``INIT``, receiving a
*SIGUSR1* signal would trigger the ``[INIT]`` sections shell script, and the state of
the icon would be changed according to the current PowerMizer state without changing
the PowerMizer setting.

LICENSE
=======================================================================================

Copyright © 2015 Stefan Göbel.

**systray-generic** is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later version.

**systray-generic** is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
**systray-generic**. If not, see <http://www.gnu.org/licenses/>.

.. :indentSize=3:tabSize=3:noTabs=true:mode=rest:maxLineLen=87: