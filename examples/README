systray-generic example configurations
========================================================================

If «make install» is used to install «systray-generic», the examples
will be installed in «$PREFIX/share/systray-generic». If, for example,
the «PREFIX» is set to «/usr/local», the examples will be installed in
«/usr/local/share/systray-generic». Both example configurations should
be usable without further modifications in this case.


cpufreq-tray
------------------------------------------------------------------------

This example will create a system tray icon that can be used to switch
the cpufreq governor between «ondemand» and «performance» (no other
governor is supported at the moment, you'll have to edit the config file
if required).

Since only root can do this, a small shell script («cpufreq-toggle») and
a «sudo» configuration file to allow every user to execute this script
as root («cpufreq-toggle.sudo») are included. When using «make install»,
the «sudo» configuration will NOT be installed in «/etc», please copy it
to the appropriate folder (or file) manually.

Paths should be fine if installed via «make install», otherwise you'll
have to fix the paths in the config files manually.

To generate a menu entry for this configuration, link the desktop file
(«cpufreq-tray.desktop») to «~/.local/share/applications/». Again, paths
in this file should be fine if installed with «make install», please
check them if not.

Note: The current governor will be determined by checking the first CPU
only. This configuration and the «cpufreq-toggle» script assume that all
CPUs are always using the same governor, and changing the governor will
affect all CPUs.


nvidia-powermizer-tray
------------------------------------------------------------------------

This example will create a system tray icon to switch the PowerMizer
state of the first Nvidia GPU between «Auto» and «Maximum Performance».

Any user running X should be able to do this, only the «nvidia-settings»
command is required. Please check that your card supports the PowerMizer
states 1 and 2 by running:

   nvidia-settings -q [gpu:0]/GPUPowerMizerMode

The output should contain the line

   Valid values for 'GPUPowerMizerMode' are: 0, 1 and 2

Use the graphical UI of «nvidia-settings» to change the PowerMizer
state, and check the output of the command above. If state 2 is auto and
state 1 is maximum performance you can use the example configuration,
otherwise you'll have to modify it according to your values. You will
also have to modify it if your GPU is not «gpu:0».

As for the cpufreq-tray example, paths in the configuration file should
be fine if installed using «make install», modify as required otherwise.

To generate a menu entry for this configuration, link the desktop file
«nvidia-powermizer-tray.desktop» to your user's applications folder
«~/.local/share/applications/». Paths in this file should be fine if
installed using «make install», otherwise you will have to modify the
desktop file.