PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/man
SHRDIR ?= $(PREFIX)/share/systray-generic
DOCDIR ?= $(PREFIX)/share/doc/systray-generic

CPUDIR ?= $(SHRDIR)/cpufreq-tray
NVPDIR ?= $(SHRDIR)/nvidia-powermizer-tray

ifeq ($(PREFIX), /usr)
	MANDIR=/usr/share/man
endif

all: man

man: man/systray-generic.1

man/systray-generic.1: doc/systray-generic.1.rst
	mkdir -p -m 0755 man
	rst2man doc/systray-generic.1.rst >man/systray-generic.1

install: man

	# Executables:
	install -d $(DESTDIR)$(BINDIR)
	install -m 755 bin/systray-generic $(DESTDIR)$(BINDIR)

	# Man pages:
	install -d $(DESTDIR)$(MANDIR)/man1
	install -m 644 man/systray-generic.1 $(DESTDIR)$(MANDIR)/man1

	# Documentation:
	install -d $(DESTDIR)$(DOCDIR)
	install -m 644 README                    $(DESTDIR)$(DOCDIR)
	install -m 644 COPYING                   $(DESTDIR)$(DOCDIR)
	install -m 644 doc/all-options.ini       $(DESTDIR)$(DOCDIR)
	install -m 644 doc/systray-generic.1.rst $(DESTDIR)$(DOCDIR)

	# cpufreq-tray example:
	install -d $(DESTDIR)$(CPUDIR)
	install -m 644 examples/cpufreq-tray/*.svg          $(DESTDIR)$(CPUDIR)
	install -m 644 examples/cpufreq-tray/*.desktop      $(DESTDIR)$(CPUDIR)
	install -m 644 examples/cpufreq-tray/*.ini          $(DESTDIR)$(CPUDIR)
	install -m 440 examples/cpufreq-tray/*.sudoers      $(DESTDIR)$(CPUDIR)
	install -m 755 examples/cpufreq-tray/cpufreq-toggle $(DESTDIR)$(CPUDIR)

	# nvidia-powermizer-tray example:
	install -d $(DESTDIR)$(NVPDIR)
	install -m 644 examples/nvidia-powermizer-tray/*.svg     $(DESTDIR)$(NVPDIR)
	install -m 644 examples/nvidia-powermizer-tray/*.desktop $(DESTDIR)$(NVPDIR)
	install -m 644 examples/nvidia-powermizer-tray/*.ini     $(DESTDIR)$(NVPDIR)

	# Readme files for the examples:
	install -m 644 examples/README* $(DESTDIR)$(SHRDIR)

	# Fix paths in some of the example files:
	sed -i "s#/usr/share#$(PREFIX)/share#" $(DESTDIR)$(CPUDIR)/*.desktop
	sed -i "s#/usr/share#$(PREFIX)/share#" $(DESTDIR)$(CPUDIR)/*.ini
	sed -i "s#/usr/share#$(PREFIX)/share#" $(DESTDIR)$(CPUDIR)/*.sudoers
	sed -i "s#/usr/share#$(PREFIX)/share#" $(DESTDIR)$(NVPDIR)/*.desktop
	sed -i "s#/usr/share#$(PREFIX)/share#" $(DESTDIR)$(NVPDIR)/*.ini

uninstall:
	rm -f  $(DESTDIR)$(BINDIR)/systray-generic
	rm -f  $(DESTDIR)$(MANDIR)/man1/systray-generic.1
	rm -rf $(DESTDIR)$(DOCDIR)
	rm -rf $(DESTDIR)$(SHRDIR)

clean:
	rm -f man/systray-generic.1
	rmdir man

.PHONY: usage man install uninstall clean

# :indentSize=3:tabSize=3:noTabs=false:mode=makefile:maxLineLen=87: